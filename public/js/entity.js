var entity_name;

function find(arr, obj) {
  for(var i=0; i < arr.length; i++){
  if(arr[i].name== obj){
  return arr[i];
  }
  }
  return null;
}

function jsonp(url, callback){
  $.ajax({
    url: url,
    dataType: "jsonp",
    jsonp: "callback",
    success: function(data){
      callback(data);
    }
  });
}

function loadEntityInfoById(id) {
  jsonp("https://events.publicvm.com:8000/v1/entities/" + id, function(data){
    setEntityInfo(data);
  });
}

function loadEntityGraphById(id) {
  jsonp("https://events.publicvm.com:8000/v1/entities/" + id + "/graph", function(data){
    renderNeo4jData(data, '#entity_info_graph');
  });
}

function loadTrendByEntity(id){
  $.ajax({
    url: "https://events.publicvm.com:8000/v1/trends/entities/" + id + "?days=30",
    dataType: "jsonp",
    jsonp: "callback",
    success: function(data){
      var rows = data['results'][1]['data'];
      var source_data = [];
      var source_xAxis = [];
      for(var i=0; i< rows.length; i++){
        var row = rows[i]['row'];
        source_xAxis.push(row[0]);
        source_data.push(row[1]);
      }

      rows = data['results'][0]['data'];
      var time_data = [];
      var time_xAxis = [];
      for(var i=0; i< rows.length; i++){
        var row = rows[i]['row'];
        time_xAxis.push(row[0]);
        time_data.push(row[1]);
      }

      Highcharts.chart('source-trend', {
          chart: {
              type: 'cylinder',
              options3d: {
                  enabled: true,
                  alpha: 15,
                  beta: 15,
                  depth: 50,
                  viewDistance: 25
              }
          },
          title: {
              text: '近一月媒体报道趋势'
          },
          xAxis: {
            categories: source_xAxis
          },
          plotOptions: {
              series: {
                  depth: 25,
                  colorByPoint: true
              }
          },
          series: [{
              data: source_data,
              name: '报道量',
              showInLegend: false
          }]
      });

      Highcharts.chart('time-trend', {
          chart: {
              type: 'cylinder',
              options3d: {
                  enabled: true,
                  alpha: 15,
                  beta: 15,
                  depth: 50,
                  viewDistance: 25
              }
          },
          title: {
              text: '近一月报道量趋势'
          },
          xAxis: {
            categories: time_xAxis
          },
          plotOptions: {
              series: {
                  depth: 25,
                  colorByPoint: true
              }
          },
          series: [{
              data: time_data,
              name: '报道量',
              showInLegend: false
          }]
      });
    }
  });
}

function loadNewsById(id) {
  $.ajax({
    url: "https://events.publicvm.com:8000/v1/entities/" + id + "/news?limit=20",
    dataType: "jsonp",
    jsonp: "callback",
    success: function(data){
      var ul_news = $('.news_list');
      ul_news.empty();
      data = data['results'][0]['data'];
      for(var i=0; i<data.length; i++){
        var news = data[i]['row'][0];
        
        var images = [];
        if(news['images'] && news['images'].length > 0){
          for(var j=0; j < news['images'].length; j++){
            var img_url = news['images'][j];
            images.push(img_url);
          }
        }else{
          images.push('img/404.gif');
        }
        
        var news_item = createNewNews({img_url:images[0], title: news['title'],source: news['source'], url:news['url'], type: news['type'], time: news['time'].substring(0,18)});
        ul_news.append(news_item);
      }
    }
  });
}

function loadEventlineById(id){
  $.ajax({
    url: "https://events.publicvm.com:8000/v1/entities/" + id + "/eventlines?limit=20",
    dataType: "jsonp",
    jsonp: "callback",
    success: function(data){
      var ul_eventline = $('.eventline_list');
      ul_eventline.empty();

      entities = [];

      var max_value = data.length > 0 ? data[0]['num'] : 1;
      for(var i=0; i<data.length; i++){
        var eventline = data[i];
        var news_list = eventline['news_list'];
        var images = [];
        for(var j=0; j< news_list.length; j++){
          var news = news_list[j];
          if(!news['images'])
            continue;
          for(var k=0; k<news['images'].length; k++){
            var img_url = news['images'][k];
            if(img_url != '')
              images.push(img_url);
          }
        }

        var title = news_list[0].title;
        
        var width_ratio = eventline['num'] / max_value * 100;
        var eventline_item = createNewEventline({width_ratio: width_ratio, eventline_id: eventline['uuid'], images: images, title: title, num_cur: eventline['num'], num_doc: eventline['num_doc'], num_event: eventline['num_event'], time: eventline['end_time'].substring(0,10), timeRange: '24H'});
        ul_eventline.append(eventline_item);
        
        for(var j=0; j < eventline['entities'].length; j++){
          var entity_name = eventline['entities'][j].name;
          var weight = eventline['entities'][j].weight;
          var obj = find(entities, entity_name);
          if(obj)
            obj.weight += weight;
          else
            entities.push({name: entity_name, weight: weight});
        }
      }

      for(var i=0; i<entities.length; i++){
        var weight = entities[i].weight;
        entities[i].weight = ((-0.7)*(weight*weight)+1.4*weight+0.3);
      }

      Highcharts.chart('wordcloud', {
          series: [{
              type: 'wordcloud',
              data: entities,
              name: '频率'
          }],
          title: {text: ''}
      });
    }
  });
}

function getNeo4jData(data, callback){
  $.ajax({
    url: "https://events.publicvm.com:8000/v1/neo4jData",
    dataType: "jsonp",
    jsonp: "callback",
    data: "statements=" + data,
    success: callback,
  });
}

function renderNeo4jData(neo4jData, element_id) {
  var neo4jd3 = new Neo4jd3(element_id, {
    highlight: [],
    minCollision: 30,
    neo4jData: neo4jData,
    nodeRadius: 20,
    onNodeDoubleClick: function(node) {
      switch(node.id) {
        default:
          
          break;
      }
    },
    onRelationshipDoubleClick: function(relationship) {
    },
    zoomFit: true
  });
}

var nameDict = {'descriptions':'描述', 'alias':'别名'}

function setEntityInfo(data){
  var properties = {}

  if (data['results'][0]['data'].length > 0) {
    var properties = data['results'][0]['data'][0]['row'][0];
    }
    var imageUrl = './img/404.gif';
    if('wikiImgUrl' in properties){
    imageUrl = properties['wikiImgUrl'];
  }

  for(key in nameDict){
    if(key in properties)  
    $('.info_list').append('<tr><td class="label">' + nameDict[key] + '</td><td><span id="entity_' + key + '">' + properties[key] + '</span></td></table>');
  }

  $('#wikipedia_link').attr('href', 'https://zh.wikipedia.org/wiki/' + entity_name);
  $('#baidu_link').attr('href', 'https://baike.baidu.com/search/word?word=' + entity_name);
  $('#wikidata_link').attr('href', 'https://www.wikidata.org/wiki/' + properties['qid']);
  $('#entity_desc').html(properties['description']);
  $('.entity_img').css('background', 'url(' + imageUrl + ') no-repeat');
  $('.entity_img').css('background-size', '100%');
  $('.entity_img').css('background-position', 'center');
}

var nav_items = $('#myTab').children(".nav-item");
var event_graph_is_loaded = false;
$('#event-graph-tab').on('click', function(e){
  if(!event_graph_is_loaded){
    event_graph_is_loaded = true;
    $('#entity_url').attr('src', 'https://events.publicvm.com:8000/entities/' + entity_name);
  }
});

var article_is_loaded = false;
  $('#article-tab').on('click', function(e){
    if(!article_is_loaded){
    article_is_loaded = true;
    loadNewsById(entity_name);
  }
});

var trend_is_loaded = false;
$('#trend-tab').on('click', function(e){
  if(!trend_is_loaded){
    trend_is_loaded = true;
    loadTrendByEntity(entity_name);
  }
});

var entity_graph_is_loaded = false;
$('#entity-graph-tab').on('click', function(e){
  if(!entity_graph_is_loaded){
    entity_graph_is_loaded = true;
    loadEntityGraphById(entity_name);
  }
});

$('#myTab li').on('click', function(e){
  for(var i=0; i < nav_items.length; i++)
    nav_items[i].classList.remove("active");
    
  $(this).addClass("active");
});

$('#myTab a').on('click', function(e){
  $(this).tab('show');
});

initDatetimePicker();

$(document).ready(function(){
  entity_name = getParameter('name');
  $('#entity_name').html(entity_name);
  $('title').html('实体: ' + entity_name);
  if(!entity_name)
    entity_name='马云';
  loadEntityInfoById(entity_name);
  loadEventlineById(entity_name);
});
