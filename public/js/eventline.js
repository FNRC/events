function getTemplate(oneNews){
    if(oneNews.img==''){
        oneNews.img="http://img5.imgtn.bdimg.com/it/u=3823263411,2792051580&fm=26&gp=0.jpg"
    }
        return        "<div class='col-md-12 one-item'>"+
                        "<div class='one-item-subitem'>"+
                            "<div class='one-item-article'>"+
                                "<div class='row'>"+
                                    "<div class='col-sm-1 col-md-2 col-lg-2 hide-on-medium'>"+
                                        "<div class='one-item-article-img'>"+
                                            "<img class='news-img' src='"+oneNews.img_url+"'>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div class='col-sm-11 col-md-10 col-lg-10 extend-on-medium'>"+
                                        "<div class='row'>"+
                                            "<div class='col-xs-8 col-md-9 col-lg-9 title-content'>"+
                                                "<h4 class='title-content-h4'>"+oneNews.title+"</h4>"+
                                                "<div class='body-text'>"+oneNews.abstract+
                                                    
                                                "</div>"+
                                            "</div>"+
                                            "<div class='col-xs-4 col-md-3 col-lg-3 article-meta'>"+
                                                "<div class='row'>"+
                                                    "<div class='col-sm-12 article-meta-line'>"+
                                                        "<a href='' class='news-source'>"+oneNews.source+"</a>"+
                                                    "</div>"+
                                                "</div>"+
                                                "<div class='row row-flex-grow'>"+
                                                    "<div class='col-sm-12 article-meta-line'>"+
                                                        "<span class='news-time'>"+oneNews.time+"</span>"+
                                                    "</div>"+
                                                "</div>"+ 
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                    "</div>" ;
    }
    
function getTimer(event){
    return      "<div data-time="+event.start_time+">"+
                    "<div class='timeline-visual'>"+
                        "<img src='http://s9.rr.itc.cn/r/wapChange/20175_15_16/a7ae4j7814412856619.jpg' alt=''>"+
                    "</div>"+
                    "<div class='timeline-detail'>"+
                        "<ul class='timeline-detail-list'>"+
                            "<li><strong>事件：</strong>"+event.title+"</li>"+
                            "<li><strong>时间：</strong>"+event.start_time+"</li>"+
                        "</ul>"+
                    "</div>"+
                "</div>"
}   

function loadNews(eventline,skip,limit){
    $.ajax(
      {
        type:"get",
        url:"https://events.publicvm.com:8000/v1/eventlines/"+eventline+"/news?skip="+skip+"&limit="+limit,
        dataType: 'jsonp',
        jsonp:"callback",
        success:function (data) {
            var data=data.results[0]['data'];
            var list=document.getElementById("list")
            for(var i=0;i<data.length;i++){
                var newnode=document.createElement("li")
                var news = data[i]['row'][0];
                news['img_url'] = './img/404.gif';
                if(news['images'])
                  news['img_url'] = changeDomain(news['images'][0]);
          
                newnode.innerHTML=getTemplate(news);
                list.appendChild(newnode);
            }

        }
      }
    );
}

function loadEventline(){
  $(function(){
    $.ajax({
      url:"https://events.publicvm.com:8000/v1/eventlines/"+eventline_id,
      dataType: 'jsonp',
      jsonp:"callback",
      success:function (data) {
        eventline = data;
        loadEvents();
      }
    })
  });
}

function loadEvents(){
  $(function(){
    $.ajax({
      url:"https://events.publicvm.com:8000/v1/eventlines/"+eventline_id+"/events",
      dataType: 'jsonp',
      jsonp:"callback",
      success:function (data) {
        data = data['results'][0]['data'];
        var events = []
        
        var eventline_images = eventline['img_url'];
        var base_imgUrl = '';
        if(eventline_images && eventline_images.length > 0)
          base_imgUrl = correctImgUrl(eventline_images[0]);
          
        for(var i=0; i < data.length; i++){
          var row = data[i]['row'];
          var event = row[0];
          var start_time = event['start_time'].replace('T', ' ').replace('Z','');
          var end_time = event['end_time'].replace('T', ' ').replace('Z','');
         
          var start_date = {year:start_time.substring(0, 4), month:start_time.substring(5, 7), day:start_time.substring(8, 10), hour: start_time.substring(11, 13), minute: start_time.substring(14, 16), second: start_time.substring(17, 19)};
          
          var end_date = {year:end_time.substring(0, 4), month:end_time.substring(5, 7), day:end_time.substring(8, 10), hour: end_time.substring(11, 13), minute: end_time.substring(14, 16), second: end_time.substring(17, 19)};
          
          var news_list = row[1];
          var title = news_list[0]['title'];
          var images = news_list[0]['images'];
          var abstract = '';
          for(var j=0; j<news_list.length; j++){
            
            if(news_list[j]['abstract']){
              abstract = news_list[j]['abstract'];
              break;
            }
          }
          
          var obj = {start_date: start_date, text: {headline: title, text:"<p>" + abstract + "</p>"}, background:{url:base_imgUrl, color: "#0d0e0ede"}, display_date: start_time + " - " + end_time};
          if(images){
            images[0] = correctImgUrl(images[0]);
            obj['media'] = {url: images[0], title: title, thumbnail: images[0], credit: "图片源自互联网"};
            obj['background']['url'] = images[0];
          }
          events.push(obj);
        }
        
        timeline = new TL.Timeline('timeline-embed', {"events":events}, {scale_factor: 0.3, start_at_end: true});
      }
    })
  })
}

function loadTrendM(eventline){
    $(function(){
        $.ajax(
            {
                type:"get",
                url:"http://223.3.76.68:8089/eventlineRelatedEvents.action?eventlineId="+eventline,
                dataType: 'jsonp',
                jsonp:"callback", 
                data:{
                },
                success:function (da) {
                    var data = [];
                    var dataSeries = { type: "spline" ,toolTipContent:"{customPropperty}:{y}"};
                    var dataPoints = [];
                    var events=da.result.allEvents
                    var alltimeline=""
                    var timer1=document.getElementsByClassName("timeline-container")[0]
                    for(var i=0;i<events.length;i++ ) {
                        alltimeline=alltimeline+getTimer(events[i])
                        dataPoints.push({
                            x:new Date(events[i].start_time),
                            y:events[i].pages,
                            customPropperty:events[i].title,
                        })
                    }

                    dataSeries.dataPoints = dataPoints;
                    // dataSeries.visible=false;
                    //dataSeries.showInLegend=true;
                    
                    // dataSeries.click=onClick,
                    dataSeries.color="#00E5EE"
                    data.push(dataSeries);
                    var options = {
                        theme:"light2",
                        zoomEnabled: true,
                        animationEnabled: true,
                        title: {
                            text: "事件线发展趋势图",
                            fontSize:16,
                        },
                        axisX:{
                            valueFormatString:"YY-MM-DD HH:mm"
                        },
                        axisY: {
                            includeZero: true,
                            // lineThickness: 1,
                            ValueFormatString:"# pages",
                        },
                        tooltip:{
                            shared:"true"
                        },
                        legend:{
                            cursor:"pointer",
                            itemclick : toggleDataSeries
                        },
                        width:1000,
                        data: data  // random data
                    };
                    
                    var chart = new CanvasJS.Chart("chartContainer", options);
                    chart.render();
                    function toggleDataSeries(e) {
                        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        chart.render();
                    }
                    function onClick(e) {
                        window.location.href = "/event.jsp?eventId="+e.dataPoint.name;
                    }

                },
                error:function () {
                    alert("系统异常，请稍后重试！")
                }
            }
        )
    });
}

function loadSource(eventline){
    
        $.ajax({
                type:"get",
                url:"http://223.3.76.68:8089/eventlineInfo.action?eventlineId="+eventline,
                dataType: 'jsonp',
                jsonp:"callback", 
                data:{
                },
                success:function(da){
                        var imgs=da.result.img
                        if(imgs.length>1){
                            $("span.eventline-img1").css("background-image","url("+changeEventlineDomain(imgs[0])+")");                        
                            $("span.eventline-img2").css("background-image","url("+changeEventlineDomain(imgs[1])+")");
                        }
                        else if(imgs.length==1){
                            $("span.eventline-img1").css("background-image","url("+changeEventlineDomain(imgs[0])+")");                        
                            $("span.eventline-img2").css("background-image","url(http://img.zcool.cn/community/018ec457c0ea470000012e7e3271da.png@900w_1l_2o_100sh.jpg)");
                        }else{
                            $("span.eventline-img1").css("background-image","url(http://img.zcool.cn/community/018ec457c0ea470000012e7e3271da.png@900w_1l_2o_100sh.jpg)");
                            $("span.eventline-img2").css("background-image","url(http://img.zcool.cn/community/018ec457c0ea470000012e7e3271da.png@900w_1l_2o_100sh.jpg)");
                        }
                        
                        

                        
                        // var eventline_img=document.getElementsByClassName("eventline-img")
                        // eventline_img.style.background-image="url("+da.result.img+")"

                        //条形图
                        var sources=da.result.sources
                        var sourcename=[]
                        var sourcenum=[]
                        
                        for(var i=0 ; i<sources.length;i++){
                            sourcename[i]=sources[i][0];
                            sourcenum[i]=Number(sources[i][1]);
                        }
                        if(sources.length<7){
                            for(var i=0;i<7-sources.length;i++){
                                sourcename[i+sources.length]=""
                                sourcenum[i+sources.length]=Number("")
                            }
                        }
                        var chart = Highcharts.chart('container3', {
                            chart: {
                                width:1000,
                                type: 'bar',
                                height:600,
                            },
                            title: {
                                text: '事件线新闻来源'
                            },
                            subtitle: {
                            },
                            xAxis: {
                                categories: sourcename,
                                title: {
                                    text: null
                                }
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: '(篇)',
                                    align: 'high'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            tooltip: {
                                valueSuffix: ' 百万'
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: false,
                                        allowOverlap: true // 允许数据标签重叠
                                    }
                                }
                            },
                            legend: {
                                enabled: false,
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'top',
                                x: -40,
                                y: 100,
                                floating: true,
                                borderWidth: 1,
                                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                                shadow: true
                            },
                            series: [{
                                data: sourcenum
                            }]
                        });
                        //事件线知识图谱
                        

                        //实体条形图
                        var entitiesStr=da.result.entities;
                        var entities=JSON.parse(entitiesStr);
                        var entity_data=[];
                        for(var i=entities.length-1;i>0;i--){
                            var obj={};
                            obj.label=entities[i][0]

                            obj.y=entities[i][1].weight
                            entity_data.push(obj)
                        }
                        var chart = new CanvasJS.Chart("entity-column", {
                            animationEnabled: true,
                            
                            width:950,
                               
                            height:600,
                            
                            title:{
                                text:"关联实体及关联度",
                                fontSize:20,
                                cornerRadius: 4,
                                
                                verticalAlign: "top",
                                horizontalAlign: "center",
                                padding:10,
                                
                            },
                            axisX:{
                                labelFontSize: 15,
                                minimum:0,
                                interval: 0.1
                            },
                            axisY2:{
                                labelFontSize: 15,
                                minimum:0,
                                includeZero: false,
                                interlacedColor: "rgba(1,77,101,.2)",
                                gridColor: "rgba(1,77,101,.1)",
                            },
                            data: [{
                                type: "bar",
                                name: "companies",
                                axisYType: "secondary",
                                color: "#014D65",
                                dataPoints: entity_data,
                                }]
                        });
                        chart.render();


                        //词云
                    
                        var entitiesStr=da.result.entities;
                        var entities=JSON.parse(entitiesStr);
                        var tt=[];
                        for(var i=0;i<entities.length;i++){
                            var obj={};
                            obj.name=entities[i][0]
                            var w=entities[i][1].weight
                            obj.weight=((-0.7)*(w*w)+1.4*w+0.3)
                            tt.push(obj)
                        }
                        Highcharts.chart('container2', {
                            chart: {
                                width:1000,
                                
                                height:600,
                            },
                            series: [{
                                type: 'wordcloud',
                                data: tt
                            }],
                            title: {
                                text: '实体词云'
                            }
                        });

                },
                error:function(){

                }
        });
}
function GetQueryString(name){
    var reg=new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r=window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
    
}
function changeDomain(source){
    if(source.search("http")!=-1){
        return source;
    }
    else{
        return "http:"+source
    }
}
function changeEventlineDomain(source){
    if(source.search("http")!=-1){
        return source;
    }
    else{
        return "http:"+source.substring(1,source.length-1)
    }
}

var eventline_id = GetQueryString("eventline");
var eventline = null;
loadEventline();
loadNews(eventline_id,1,5)
loadTrendM(eventline_id)
loadSource(eventline_id)
function getNeo4jData(data, callback){
    $.ajax({
        url: "https://events.publicvm.com:8000/v1/neo4jData",
        dataType: "jsonp",
        jsonp: "callback",
        data: "statements=" + data,
        success: callback,
    });
}

function renderNeo4jData(neo4jData, element_id, query_statements) {
    var neo4jd3 = new Neo4jd3(element_id, {
        zoomFit: true,
        highlight: [],
        minCollision: 30,
        neo4jData: neo4jData,
        nodeRadius: 40,
        onNodeDoubleClick: function(node) {
            switch(node.id) {
                default:
                post(query_statements, function(data){
                    neo4jd3.updateWithNeo4jData(data);
                });
                break;
            }
        },
        onRelationshipDoubleClick: function(relationship) {
        },
        
        height:500
    });
}

var statements = JSON.stringify({"statements": [{"statement":"MATCH (p:Eventline)-[r:Associates]->(e) WHERE p.uuid='"+GetQueryString("eventline")+"'with p, collect(r) as relations, collect(e) as entities, count(r) as num order by num desc limit 1 RETURN p,relations,entities", "resultDataContents": [ "graph" ]}]});
getNeo4jData(statements, function(data){
                                //setEntityInfo(data);
                                renderNeo4jData(data, '#neo4jd3', statements);
                            });



