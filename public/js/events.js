var counter_news = 0;

function bubleSort(arr){
  var len = arr.length;
  for(var i=0; i < len - 1; i++){
    var max = arr[0];
    var max_index = 0;
    for (var j=1; j < len -i ; j++) {
      if (max[1]['pos'] < arr[j][1]['pos'] || (max[1]['pos']==arr[j][1]['pos'] && max[1]['weight'] >= arr[j][1]['weight'])){
        max = arr[j];
        max_index = j;
      }
    }
    arr[max_index] = arr[len -i -1];
    max[1]['index'] = len -i - 1;
    arr[len -i -1] = max;
  }
  arr[0][1]['index'] = 0;
  return arr;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

 
// 随机数函数
function random(min, max) {
    return parseInt((max - min) * Math.random());
}
function renderHotEventlines(hot_eventlines){
  var series = [];
  for(var i=0; i < hot_eventlines.relatedUCL.length; i++){
    var obj = {name: '', type: 'spline', showInLegend: true,　dataPoints:[]};
    
    var eventline = hot_eventlines.relatedUCL[i];
    var keywords = JSON.parse(eventline[2]); // 解析关键词
    var keeped_keywords = [keywords[0]];
    var sum = 0;
    var keeped_num = 15;
    for (var j=1; j < keywords.length; j++){
      var weight = keywords[j][1]['weight'];
      var oldVal = keywords[j-1][1]['weight'];
      if(j > keeped_num || weight < 0.01){
        break;
      }
      sum += weight;
      keeped_keywords.push(keywords[j]);
    }
    
    bubleSort(keeped_keywords);
    
    var left_word_num = 2;
    var right_word_num = 2;
    var count = 0;
    for(var j=keywords[0][1]['index']-1; j >= 0; j--){
      var keyword=keeped_keywords[j];
      var before_keyword = keeped_keywords[j+1];
      diff_pos = Math.abs(keyword[1]['pos']-before_keyword[1]['pos']);
      diff_weight = Math.abs(before_keyword[1]['weight'] - keyword[1]['weight']);
      diff_ratio = diff_weight / before_keyword[1]['weight'];
      
      if((keyword[0].length==1&&diff_pos!=0) || (diff_pos == 0 && diff_ratio < 0.2)){
        continue;
      }

      if((diff_pos == 0 && diff_ratio > 0.26) || diff_pos > 1)
        obj['name'] = ' ' + obj['name'];
      obj['name'] = keyword[0] + obj['name'];
      count ++;
      if(count == left_word_num){
        break;
      }
    }
	
    obj['name'] += keywords[0][0];
    count = 0;
    for(var j=keywords[0][1]['index']+1; j < keeped_keywords.length; j++){
      var keyword=keeped_keywords[j];
      var before_keyword = keeped_keywords[j-1];
      diff_pos = keyword[1]['pos']-before_keyword[1]['pos'];
      diff_weight = Math.abs(before_keyword[1]['weight'] - keyword[1]['weight']);
      diff_ratio = diff_weight / before_keyword[1]['weight'];
      
      if((keyword[0].length==1&&diff_pos!=0) || (diff_pos == 0 && diff_ratio < 0.2)){
        continue;
      }

      if((diff_pos == 0 && diff_ratio > 0.26) || diff_pos > 1)
        obj['name'] += ' ';
      obj['name'] += keyword[0];
      count ++;
      if(count == right_word_num){
        break;
      }
    }
    obj['name'] ="事件线"+(i+1)+ ":(" + obj['name'] + ")";

    /*
    obj['name'] = keeped_keywords[0][0];
    var word_num = 1;
    for(var j=1; j< keeped_keywords.length; j++){
      var keyword=keeped_keywords[j];
      var before_keyword = keeped_keywords[j-1];
      diff_pos = keyword[1]['pos']-before_keyword[1]['pos'];
      diff_tfidf = before_keyword[1]['tf-idf'] - keyword[1]['tf-idf'];
      diff_ratio = diff_tfidf > 0 ? diff_tfidf / before_keyword[1]['tf-idf'] : -1 * diff_tfidf / keyword[1]['tf-idf'];
      
      if(keyword[0].length==1 || (diff_pos == 0 && diff_ratio < 0.2)){
        continue;
      }

      if((diff_pos == 0 && diff_ratio > 0.26) || diff_pos > 1)
        obj['name'] += ' ';
      obj['name'] += keyword[0];
      word_num+=1;
      if (word_num ==4)
        break;
    }
  　 */
  　 var c = ["新华网", "中新网", "网易新闻", "新浪新闻", "澎湃网"];
    var events = eventline.relatedUCL;
    for(var j=0; j < events.length; j++){
      var event = events[j];
      var event_info = JSON.parse(event[11]);
      obj['dataPoints'].push({x: new Date(event_info['time']), y: event_info['num_doc'], title: event[1]+'('+c[random(0, c.length - 1)]+')'});
    }
    
    obj.toolTipContent='{y}篇， {title}';
    series.push(obj);
  }

  var chart = new CanvasJS.Chart("hot-eventlines-chart", {
    theme:"light2",
    animationEnabled: true,
    zoomEnabled: true,
    title:{
      fontSize: 18
    },
    toolTip:{
      shared: true,
    },
    axisY :{
      includeZero: false,
      title: "报道数",
    },
    legend:{
      cursor:"pointer",
      itemclick : toggleDataSeries
    },
    data: series,
  });
  chart.render();

  function toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
      e.dataSeries.visible = false;
    } else {
      e.dataSeries.visible = true;
    }
    chart.render();
  }
}
