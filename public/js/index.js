//滚动插件
(function($){
  $.fn.extend({
    Scroll:function(opt,callback){
      //参数初始化
      if(!opt) var opt={};
      var _this=this.eq(0).find("ul:first");
      var lineH=_this.find("li:first").height(), //获取行高
          line=opt.line?parseInt(opt.line,10):parseInt(this.height()/lineH,10), //每次滚动的行数，默认为一屏，即父容器高度
          speed=opt.speed?parseInt(opt.speed,10):500, //卷动速度，数值越大，速度越慢（毫秒）
          timer=opt.timer?parseInt(opt.timer,10):3000; //滚动的时间间隔（毫秒）
      
      if(line===0) line=1;
      var upHeight=line*lineH;
      //滚动函数
      scrollUp=function(){
        if(_this.find("li").length < 6)
          return;
        _this.animate({
          marginTop:upHeight
        },speed,function(){
          for(i=1;i<=line;i++){
            _this.find("li:last").prependTo(_this);
          }
          _this.css({marginTop:0});
        });
      };
      //鼠标事件绑定
      _this.hover(function(){
        clearInterval(timerID);
      },function(){
        timerID=setInterval("scrollUp()",timer);
      }).mouseout();
    }
  });
})(jQuery);

function loadHotEventlines(skip, limit){
  var ul_eventline = $('.eventline_list');
  var new_ele = $('<img src="./img/loading.gif" style="margin: auto; height: 80px;">');
  ul_eventline.append(new_ele);
  if(!skip)
    skip = 0;
  if(!limit)
    limit = 16;
  $.ajax({
    type:"get",
    url:"https://events.publicvm.com:8000/v1/trends/eventlines?start_time=" + start_time + "&end_time=" + end_time + "&skip=" + skip + "&limit=" + limit,
    dataType: 'jsonp',
    jsonp:"callback",
    success:function (eventlines) {
      new_ele.remove();
      num_eventline += eventlines.length;
      
      var max_value = eventlines.length > 0 ? eventlines[0]['num'] : 1;
      for(var i=0; i < eventlines.length; i++){
        var eventline = eventlines[i];
          
        var news_list = eventline['news_list'];
        var images = [];
        for(var j=0; j< news_list.length; j++){
          var news = news_list[j];
          if(!news['images'])
            continue;
          for(var k=0; k<news['images'].length; k++){
            var img_url = news['images'][k];
            if(img_url != '')
              images.push(img_url);
          }
        }
        
        var entities = eventline['entities'];
        var locations = "";
        for(var j=0; j< entities.length; j++) {
          var type = entities[j]['type'];
          if(type=="LOC" || type=="ORG")
            locations += entities[j]['name'] + " ";
          if(j>=2)
            break;
        }
    
        var title = news_list[0].title;        
        
        var width_ratio = Math.round(eventline['num'] / max_value * 100);
        var eventline_item = createNewEventline({width_ratio: width_ratio, eventline_id: eventline['uuid'], images:images, title: title, locations: locations, num_cur: eventline['num'], num_doc: eventline['num_doc'], num_event: eventline['num_event'], time: eventline['time'].substring(0,10), start_time: eventline['start_time'].substring(0,10), timeRange: timeRange});
        var ele = ul_eventline.find('#'+eventline['uuid']);

        if(ele.length > 0){
          ele.parent().replaceWith(eventline_item);
        }else{
          ul_eventline.append(eventline_item);
        }
        addEventlineItemEventlistener(eventline['uuid']);
      }
      
      updatePosition();
      updateNavFloat();
    }  
  });
}

function reRenderEventlines(data){
  renderEventlineByUCL(data);
}

function addEventlineItemEventlistener(id){
  var target = $("#"+id);
    
  target.mouseenter(function(){
    $(this).children('.follow-me').css('display', 'flex');
  });
  
  target.mouseleave(function(){
    $(this).children('.follow-me').css('display', 'none');
  });
  
  target.on('click', function(e){
    window.open('./eventline.html?eventline=' + id);
  })
  
  var btn_unfollow = target.find('.follow-me .unfollow')
  btn_unfollow.on('click', function(e){
    e.stopPropagation();
  });
  
  var btn_follow = target.find('.follow-me .follow')
  btn_follow.on('click', function(e){
    e.stopPropagation();
  });
}

function renderTopTimeline(eventline_id, eventline_images){
  $(function(){
    $.ajax({
      url:"https://events.publicvm.com:8000/v1/eventlines/"+eventline_id+"/events",
      dataType: 'jsonp',
      jsonp:"callback",
      success:function (data) {
        data = data['results'][0]['data'];
        var events = []
        
        var base_imgUrl = '';
        if(eventline_images && eventline_images.length > 0)
          base_imgUrl = eventline_images[0];
          
        for(var i=0; i < data.length; i++){
          var row = data[i]['row'];
          var event = row[0];
          var start_time = event['start_time'].replace('T', ' ').replace('Z','');
          var end_time = event['end_time'].replace('T', ' ').replace('Z','');
         
          var start_date = {year:start_time.substring(0, 4), month:start_time.substring(5, 7), day:start_time.substring(8, 10), hour: start_time.substring(11, 13), minute: start_time.substring(14, 16), second: start_time.substring(17, 19)};
          
          var end_date = {year:end_time.substring(0, 4), month:end_time.substring(5, 7), day:end_time.substring(8, 10), hour: end_time.substring(11, 13), minute: end_time.substring(14, 16), second: end_time.substring(17, 19)};
          
          var news_list = row[1];
          var title = news_list[0]['title'];
          var images = news_list[0]['images'];
          var abstract = '';
          for(var j=0; j<news_list.length; j++){
            
            if(news_list[j]['abstract']){
              abstract = news_list[j]['abstract'];
              break;
            }
          }
          
          var obj = {start_date: start_date, text: {headline: title, text:"<p>" + abstract + "</p>"}, background:{url:base_imgUrl, color: "#0d0e0ede"}, display_date: start_time + " - " + end_time};
          if(images){
            images[0] = correctImgUrl(images[0]);
            obj['media'] = {url: images[0], title: title, thumbnail: images[0], credit: "图片源自互联网"};
            obj['background']['url'] = images[0];
          }
          events.push(obj);
        }
        
        timeline = new TL.Timeline('timeline-embed', {events:events}, {scale_factor: 0.3, start_at_end: true});
      }
    })
  })
}

function renderEventlineByUCL(data){
  if(data.relatedUCL.length == 0)
    return;

  var ul_eventline = $('.eventline_list');
  if (num_eventline == 0){
    num_eventline = Math.min(num_display_el, data.relatedUCL.length);
    ul_eventline.empty(); 
    $('#btn-eventline').css('display', 'block');
  }
  var children = ul_eventline.children();
  
  var max_value = data.relatedUCL[0][11]['num'];
  for(var i=0; i < data.relatedUCL.length; i++){
    if(i>=num_display_el)
      break;
	  
    var eventline = JSON.parse(data.relatedUCL[i][11]);
    var entities = JSON.parse(data.relatedUCL[i][5]);
    var locations = "";
    for(var j=0; j< entities.length; j++) {
      var type = entities[j]['type'];
      if(type=="LOC" || type=="ORG")
        locations += entities[j]['name'] + " ";
      if(j>=2)
        break;
    }

    var news_list = eventline['news_list'];
    var images = [];
    for(var j=0; j< news_list.length; j++){
      var news = news_list[j];
      if(!news['images'])
        continue;
      for(var k=0; k<news['images'].length; k++){
        var img_url = news['images'][k];
        if(img_url != '')
          images.push(img_url);
      }
    }

    var title = news_list[0].title;
    var width_ratio = Math.round(eventline['num'] / max_value * 100);
    
    if(i==0){
      renderTopTimeline(eventline['id'], images);
    }

    var eventline_item = createNewEventline({width_ratio: width_ratio, eventline_id: eventline['id'], images: images, title: title, locations: locations, num_cur: eventline['num'], num_doc: eventline['num_doc'], num_event: eventline['num_event'], time: eventline['time'].substring(0,10), start_time: eventline['start_time'].substring(0,10), timeRange: timeRange});
    el = children.get(i);
    if(el){
      $(el).replaceWith(eventline_item);
    }else
      ul_eventline.append(eventline_item);
    
    addEventlineItemEventlistener(eventline['id']);
  }
  
  updatePosition();
  updateNavFloat();
}

function updateEventlineByUCL(data){
  var ul_eventline = $('.eventline_list');
  for(var i=0; i < 16; i++){
    var eventline = JSON.parse(data.relatedUCL[i][11]);
    var el_eventline = ul_eventline.find('#' + eventline['id']);
    if(el_eventline){
      el_eventline.find('#num_cur').html(eventline['num']);
      el_eventline.find('#num_doc').html(eventline['num_doc']);
      el_eventline.find('#num_event').html(eventline['num_event']);
    }
  }
}

function loadHotEntity(element_id, type, limit, start_time, end_time){
  $.ajax({
    type:"get",
    url:"https://events.publicvm.com:8000/v1/trends/entities",
    dataType: 'jsonp',
    data: {type:type, limit:limit, start_time: start_time, end_time: end_time},
    jsonp:"callback",
    success:function (data) {
      var element = $(element_id);
      if (num_entity == 0){
        element.empty();
        $('btn-entity').css('display', 'block');
      }
      num_entity += data.length;
      
      var element = $(element_id);
      element.empty();
      
      var default_img = '';
      if(type=='LOC')
        default_img = './img/entity_loc.jpg';
      else if(type=='ORG')
        default_img = './img/entity_org.jpg';
      else
        default_img = './img/entity_person.jpg';
      
      var data = data['results'][0]['data'];
      var max_value = data.length > 0 ? data[0]['row'][1] : 1;
      for(var i=0;i<data.length;i++){
        var name = data[i]['row'][0];
        var weight = data[i]['row'][1];
        var imgUrl = data[i]['row'][2];
        if(!imgUrl)
          imgUrl = default_img;
        var width_ratio = Math.round(weight / max_value * 100);
        var entity_li = template_entity.format({name: name, img: imgUrl, width_ratio: width_ratio});
        element.append(entity_li);
      }
      
      updatePosition();
      updateNavFloat();
    }
  });
}

function loadStatisticalInfo(){
  $.ajax({
    type:"get",
    url:"https://events.publicvm.com:8000/v1/statistics?start_time=" + start_time +  "&end_time="+end_time,
    dataType: 'jsonp',
    jsonp:"callback",
    success:function (data) {
      var ids = ['num_news', 'num_event', 'num_eventline'];
      var chart_ids = ['news-trend', 'event-trend', 'eventline-trend'];
      var nums = [];
      var counts = [];
      var elements = [];
      var max_num = 0;
      var charts = [];
      for(var i=0; i<ids.length; i++) {
        nums.push(data['results'][i]['data']);
        counts.push([0, 0]);
        elements.push($('#'+ids[i]));
        
        var trend_ele = $('#' + chart_ids[i]);
        trend_ele.next().css('display', 'none')
        var ctx = trend_ele.get(0).getContext('2d');
        var chart = new Chart(ctx, {
          type: 'line',
          data: {
            datasets: [{
              label: '数量',
              backgroundColor: "rgba(255, 255, 255, 0.5)",
              borderColor: '#fff',
              borderWidth: 1.5
            }]
          },
          options: {
            legend: {
              display: false
            },
            animation: {
            },
            scales: {
              xAxes: [{
                  display: false,
                  gridLines: { display: false}
              }],
              yAxes: [{
                  display: false,
                  gridLines: { display: false},
                  ticks: {
                      beginAtZero:true
                  }
              }]
            }
          }
        });
        charts.push(chart);
      }
      
      var i = 0;
      var interval = setInterval(function(){
        if(i<elements.length) {
          for(var j=0; j < elements.length; j++){
            var count = counts[j];
            if(count[0] >= nums[j].length)
              continue;
            var point = nums[j][count[0]]['row'];
            count[1] += point[1];
            elements[j].html(count[1]);
            charts[j].data.labels.push(point[0]+":00");
            charts[j].data.datasets.forEach((dataset) => {
              dataset.data.push([point[1]]);
            });
            charts[j].update();
            count[0] ++;
            if(count[0] >= nums[j].length)
              i++;
          }
        }else{
          clearInterval(interval)
        }
      }, 30);
      
      updatePosition();
      updateNavFloat();
   }
  });
}

function loadNews(skip, limit){
  var ul_recent_news = $(".latest-news");
  var new_ele = $('<img src="./img/loading.gif" style="display:block; width: 80px; margin: auto;">');
  ul_recent_news.append(new_ele);
  news_is_loaded = false;
  $.ajax({
    type:"get",
    url:"https://events.publicvm.com:8000/v1/news?skip="+skip+"&limit="+limit,
    dataType: 'jsonp',
    jsonp:"callback",
    success:function (data) {
      data = data['results'][0]['data'];
      new_ele.remove();
      for(var i=0;i<data.length;i++){
        var news = data[i]['row'][0];
        var eventlines = data[i]['row'][1];
        
        var images = [];
        if(news.images && news.images.length > 0){
          for(var j=0; j < news.images.length; j++){
            var image = news.images[j];
            if(image!="")
              images.push(image);
          }
        }else
          images.push('img/404.gif');
        
        var title = news.title;
        var url = news.url;
        var type =news.type;
        if(!type)
          type='未知';
        
        var source = news.source;
        var time = news.time;
        var html_li = template_news.format({img_url: images[0], url: url, title:title, type:type, source: source, time: time, eventline_id: eventlines[0]});
     
        updateScrollDiv(url, title);
        ul_recent_news.append(html_li);
      }

      news_is_loaded = true;
    }
  });
}

function updateScrollDiv(url, title, type){
  var cls="odd";
  var status = '<span class="status status_new">新</span>';
  if(counter_news % 2==0)
    cls="even";
  if(!type)
    status='';
  else if(type != '0')
    status = '<span class="status status_hot">热</span>';
  $("#scrollDiv ul").prepend('<li><a href="' + url + '" title="' + title + '" target="_blank">' + title + '</a>'+status+'</li>');
  
  if(counter_news > 10) {
    $("#scrollDiv ul").remove("li:last");
  }
  counter_news += 1;
}

function updateNewAndHotEvents(data){
  for(var i=0; i<data.relatedUCL.length; i++){
    var news = data.relatedUCL[i];
    var title = news[1];
    var url = news[2];
    var type = news[6];
    
    var id = news[0];
    var source = news[3];
    var time = news[4];
    var img_urls = JSON.parse(news[5]);
    var type = news[7];
    if(type=='null')
          type='未知';
    
    updateScrollDiv(url, title, type);
    var img_url = 'img/404.gif';
    if(img_urls && img_urls.length > 0){
      img_url=img_urls[0];
      var header=img_url.substring(0,4);
      if(header!="http"){
        img_url="http:"+img_url;
      }
    }
    $(".latest-news").prepend(template_news.format({img_url: img_url, url: url, title:title, type:type, source: source, time: time, eventline_id: ""}));    
  }
}

globalData = null;
function handleMessage(e, ws) {
  var data = new UCLPropertySet();
  data.unpack(new Uint8Array(e.data));
  globalData = data;
  console.log("UCL对象: ", data);
  if(data.getType()==0){ // eventline push 
    reRenderEventlines(data);
    renderHotEventlines(data);
  }else if(data.getType()==3){ // news push
    updateNewAndHotEvents(data);
    if(first_time)
      first_time=false;
  　 else　{
      loadStatisticalInfo();
      loadEntities();
    }
  }
}

function parseTime(){
  start_time = getParameter('start_time');
  end_time = getParameter('end_time');
  limit = getParameter('limit');
  var now = new Date().getTime();
  if(start_time&&!end_time) {
    if(start_time.indexOf('T')<0)
      start_time += 'T00:00:00';
    var ts = new Date(start_time).getTime();
    start_time = time2str(ts);
    end_time = time2str(ts+ 86400000);
  }else if(end_time&&!start_time){
    if(end_time.indexOf('T')<0)
      end_time += 'T00:00:00';
    var ts = new Date(end_time).getTime();
    start_time = time2str(ts - 86400000)
    end_time = time2str(ts);
  }
  
  if(start_time && end_time){
    var start_timestamp = Date.parse(new Date(start_time));
    var end_timestamp = Date.parse(new Date(end_time));
    var second = (end_timestamp - start_timestamp)/1000;
    
    if(second < 60) {
      timeRange = second + "S";
    }else if(second < 3600) {
      timeRange = parseInt(second/60) + "M";
    }else
      timeRange = parseInt(second/3600) + "H";
  }

  if(limit || (start_time && end_time)){
    $('.hot-eventlines .floor-name').html("TOP" + (limit ? limit:5) + "事件趋势");
  }
}

function loadEntities(){
  loadHotEntity('#person_list', 'PER', 10, start_time, end_time);
  loadHotEntity('#orgnization_list','ORG',10, start_time, end_time);
  loadHotEntity('#location_list', 'LOC', 10, start_time, end_time);
}

function initNavFloat(){
  var y = $(".container").offset().left;

  if(!start_time && !end_time){
    $('.time-tip').css('left', y - 80);
    $('.time-tip').css('display', 'block');
  }
  $('.nav-float').css('right', y-51);
  $('.nav-float').css('display', "block");
}

function updateNavFloat(){
  var pos_navFloat = $('.nav-float').offset().top;
  var children = $('.nav-float').children();
  for(var i=0; i<children.length; i++){
    children[i].classList.remove('bg-blue');
  }

  var index = 0;
  if(pos_navFloat < pos_entity)
    index = 0;
  else if(pos_navFloat < pos_eventline) {
    index = 1;
  }else if(pos_navFloat < pos_news) {
    index = 2;
  }else {
    index = 3;
  }
  $(children[index]).addClass('bg-blue');
}

function updatePosition(){
  pos_entity = $('#anchor_entity').offset().top;
  pos_eventline = $('#anchor_eventline').offset().top;
  pos_news = $('#anchor_news').offset().top;
}

var start_time = null;
var end_time = null;
var limit = null;
var timeRange = "24H";
var first_time = true;

parseTime();
initNavFloat();
initDatetimePicker();

var num_eventline = 0;
var num_entity = 0;
var num_display_el = 24;
var num_display_en = 10;
var news_is_loaded = false;

var pos_entity = 0;
var pos_eventline = 0;
var pos_news = 0;
var seconds = 0;

$(document).ready(function(){
  updatePosition();
  updateNavFloat();
  
  loadStatisticalInfo();
  loadEntities();
  
  if(window.WebSocket){
    var ws = new WebSocket('wss://events.publicvm.com:8000');
    ws.binaryType='arraybuffer';
    ws.onopen = function(e){
      console.log("连接服务器成功，加载数据中");
      params = {"path":"hot-events"};
      params['start_time'] = start_time;
      params['end_time'] = end_time;
      params['limit'] = getParameter('limit');
      ws.send(JSON.stringify(params));
    }
    ws.onclose = function(e){
      console.log("服务器关闭");
    }
    ws.onerror = function(){
      console.log("连接出错");
    }
    ws.onmessage = function(e){
      seconds = 0;
      setTimeout(function(){handleMessage(e, ws)}, 0);
    }
  }
  
  loadNews(0,10);

  $("#scrollDiv").Scroll({line:1,speed:500,timer:2000});//可修改line值，speed值，timer值
  /**Scroll Action**/
  var last_position = 0;
  var last_time = 0;
  var news_count = 1;
  $(window).scroll(function(){
    var scrollTop = $(document).scrollTop();
    var cur_time = new Date().getTime();
    
    updateNavFloat();

    if(news_is_loaded && news_count < 6 && last_position < scrollTop-100 && scrollTop >= $(document).height() - $(window).height()-100){
      last_position = scrollTop;
      last_time = cur_time;
      news_count ++;
      loadNews((news_count-1)*10,10)
    }else if(news_is_loaded && news_count >=6){
      $('#news_end').css('display', 'block');
    }
  });
  
  var it = setInterval(function(){
    seconds += 1;
    if(seconds > 600){
      $('#seconds').html('刷新一下</br>页面吧');
      clearInterval(it);
    }else{
      $('#seconds').html(seconds+'秒前<br/>更新');
    }
  }, 1000);
});

$("#btn-eventline").on('click', function(e){
  loadHotEventlines(num_eventline, 16);
});

$(window).resize(function(){
  initNavFloat();
});
