CDPS = {
	id: 10,
	keywords: 2,
	entities: 5,
	desc: 11,
	events: 14,
}

var textDecoder = (typeof TextDecoder) != 'undefined' ? new TextDecoder('utf-8') : null;
var decodeUTF8 = textDecoder ? function(bytes){ 
	return textDecoder.decode(bytes);} : 
	function(bytes){
          var encoded="";
  for (var i=0; i < bytes.length; i++){
    encoded += '%' + bytes[i].toString(16);
  }
  return decodeURIComponent(encoded);
}

function computeByteNum(len){
    var bytes = int2byte4(len);
    var i=4;
    for(; i>0; i--){
        if(bytes[i-1] > 0){
            len += i;
            if(i<4 && len >> i*8 > 0){
                i=i+1;
            }
            break;
        }
    }
    return i;
}

function int2byte4(number) {
	var bytes = [];
	bytes[0] = number & 0XFF;
	bytes[1] = number >> 8 & 0XFF;
	bytes[2] = number >> 16 & 0XFF;
	bytes[3] = number >> 24 & 0XFF;
	return bytes;
}

function TLV(value, number, parseRule){
	if(!number)
		number = 0;
	if(!parseRule)
		parseRule = 0;
	this.updated = false;
	TLV.prototype.setNumber.call(this, number);
	TLV.prototype.setParseRule.call(this, parseRule);
	TLV.prototype.setValue.call(this, value);
}

TLV.prototype.setNumber = function(number) {
    this.type = this.type & 0XF0 | (number & 0X0F);
	this.updated = false;
}

TLV.prototype.setParseRule = function(parseRule) {
    this.type = this.type & 0X0F | (parseRule << 4);
	this.updated = false;
}

TLV.prototype.setValue = function(value) {
	if(!value)
		return;
	this.value = Array.from(value);
	this.updated = false;
}

TLV.prototype.getType = function() {
	return this.type;
}

TLV.prototype.getNumber = function(){
	return this.type & 0X0F;
}

TLV.prototype.getBytes = function(){
	if(this.updated)
		return this.bytes;
	else
		return this.pack();
}

TLV.prototype.pack = async function() {
	var num_bytes = computeByteNum(2 + this.value.length);
	var total_size = 2 + num_bytes + this.value.length;
	var bytes_len_info = int2byte4(total_size);

	this.len = [(num_bytes-1) & 0XFF];
	this.len = this.len.concat(bytes_len_info.slice(0, num_bytes));
	this.bytes = [this.type];
	this.bytes = this.bytes.concat(this.len);
	this.bytes = this.bytes.concat(this.value);
	this.updated = true;
	return this.bytes;
}

TLV.prototype.unpack = function(bytes){
	this.bytes = bytes;
	this.type = bytes[0];
	var len = (bytes[1] & 0x03) + 1;
	var size = 0;
	for(var i=len-1; i>=0; i--){
		size = size * 256 + (bytes[i+2]&0XFF);
	}
	if(bytes.length != size) {
		throw "UCL属性格式错误!"
	}
	size -= len　+ 2;
	this.len = bytes.slice(1, len + 2);
	this.value = bytes.slice(len+2)
}

function UCLPropertySet(number) {
	TLV.call(this, null, number, null);
	this.tlvSet = {};	
	this.relatedUCL = [];
}

UCLPropertySet.prototype.__proto__ = TLV.prototype;
UCLPropertySet.prototype.addProperty = function(number, value){
	var tlv = new TLV(value, number);
    this.tlvSet[tlv.getNumber()]=tlv;
}

UCLPropertySet.prototype.addRelatedUCL = function(ups){
	this.relatedUCL.push(ups);
}

UCLPropertySet.prototype.pack = async function(){
	var num_ele = Object.keys(this.tlvSet).length; //元素数量
    var quick_matcher = 0;  //快速匹配项
	if(this.relatedUCL.length > 0) {
        for (var i=0; i < this.relatedUCL.length; i++){
			this.relatedUCL[i].updated = false;
			this.relatedUCL[i].pack();
        }
    }

	for(index in this.tlvSet) {
		this.tlvSet[index].updated = false;
        this.tlvSet[index].pack();
    }
	
	if(this.relatedUCL.length > 0) {
		var bytes = [];
        for (var i=0; i < this.relatedUCL.length;){
			if(this.relatedUCL[i].updated){
				bytes = bytes.concat(this.relatedUCL[i].bytes)
				i++;
			}
        }
		this.tlvSet[14] = new TLV(bytes, 14);
		this.tlvSet[14].pack();
    }

	this.value = [];
    for(index in this.tlvSet) {
		quick_matcher = quick_matcher | (1<<index);
        var tlv = this.tlvSet[index];
		while(!tlv.updated){}
        this.value = this.value.concat(tlv.getBytes());
    }

    var num_bytes = computeByteNum(4 + this.value.length);
    var total_size = 4 + num_bytes + this.value.length;
    var bytes_len_info = int2byte4(total_size);

    // 设置元素数量和字节数量
    this.len　= [(num_ele << 2) & 0xFC | (num_bytes - 1)];

    // 设置长度信息
	this.len = this.len.concat(bytes_len_info.slice(0, num_bytes));

    // 设置快速匹配项
    this.len.push(quick_matcher>>8);
    this.len.push(quick_matcher&0XFF);

    this.bytes = [this.type]; // 设置类型信息
	this.bytes = this.bytes.concat(this.len)
	this.bytes = this.bytes.concat(this.value);
	this.updated = true;
    return this.bytes;
}

UCLPropertySet.prototype.unpack = function(bytes){
	this.tlvSet = {};
	this.bytes = bytes; // UCL二进制数据
	this.type = bytes[0]; // UCL属性集合类型
	var len = (bytes[1] & 0x03) + 1; // 长度信息占字节数
	var size = 0; // 属性集合总长度
	for(var i=len-1; i>=0; i--) {
		size = size * 256 + (bytes[i+2]&0xFF);
	}
	if(bytes.length != size) {
		throw "UCL属性格式错误!";
	}
	//size -= len + 4; // 属性集合value部分大小
	this.len = bytes.slice(1, len + 4); // 解析并更新长度域
	this.value = bytes.slice(len + 4); // 解析并更新值域

	for(var j=0; j < this.value.length;) {
		var number = this.value[j] & 0x0F;　// 解析TLV类型信息
		var num_bytes = (this.value[j+1] & 0x3) + 1; // 解析字节数
		
		size = 0; // TLV总长度
		for(var k=num_bytes-1; k >=0 ; k--) {
			size = size * 256 + (this.value[j+k+2]&0XFF);
		}
		var sub_bytes = this.value.slice(j, j + size); // 解析TLV二进制数据
		var tlv = new TLV();
		tlv.unpack(sub_bytes); // 解析TLV数据
		this.tlvSet[number] = tlv;
		if(number!=14)
			this[number] = decodeUTF8(tlv['value']);
		j += size;
	}
	if(this.tlvSet[14] != undefined){
		this.relatedUCL = []
		var value = this.tlvSet[14]['value'];
		for(var j=0; j < value.length;) {
			var number = value[j]& 0x0F; // UCL属性集合类型
			var len = (value[j+1] & 0x03) + 1; // 长度信息占字节数
			var size = 0;
			for(var i=len-1; i>=0; i--) {
				size = size * 256 + (value[i+j+2]&0XFF);
			}
			var sub_bytes = value.slice(j, j+size); // 解析并更新值域
			var ups = new UCLPropertySet();
			ups.unpack(sub_bytes);
			this.relatedUCL.push(ups);
			j += size;
		}
	}
}
