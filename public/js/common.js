var paramMap={};
var template_entity = '\
  <li class="entity_item">\
    <a href="entity.html?name={name}" title="{name} 热度: {width_ratio}">\
      <span style="background:' + "url('{img}')" + ' no-repeat; background-size: cover;"\ class="entity_img"><!-- --></span>\
      <div class="entity_desc">\
        <span class="entity_title">{name}</span>\
        <div class="line" style="width:{width_ratio}%"></div>\
      </div>\
    </a>\
  </li>';

var template_news = '\
  <li>\
    <div class="whole-article">\
      <div class="article-img" style="' + "background: url('{img_url}')" + ' no-repeat; background-size: cover;"></div>\
      <div class="single-mode">\
        <div class="single-mode-inner">\
          <div class="single-mode-inner-title">\
            <a href="{url}" title="{title}" target="_blank">{title}</a>\
          </div>\
          <div><span class="eventline_id">事件线: <a href="./eventline.html?eventline={eventline_id}">{eventline_id}</a></span></div>\
          <div class="footer-bar">\
            <div class="footer-bar-left">\
              <span class="type">{type}</span>\
              <span class="source">{source}</span>\
              <span class="time">{time}</span>\
            </div>\
          </div>\
        </div>\
      </div>\
    </div>\
  </li>';

var template_eventline_img_3 = '\
          <div class="col-7">\
            <div style="height:120px; max-height: 120px">\
              <img src="{img_url}" onerror="img_not_found()">\
            </div>\
          </div>\
          <div class="col-5">\
            <div class="row" style="margin: unset; height: 60px; max-height: 60px;">\
              <img src="{img_url_2}" onerror="img_not_found()">\
            </div>\
            <div class="row" style="margin: unset; height: 60px; max-height: 60px;">\
              <img src="{img_url_3}" onerror="img_not_found()">\
            </div>\
          </div>'

var template_eventline_img_2 = '\
          <div class="col-7">\
            <div style="height:120px; max-height: 120px">\
              <img src="{img_url}" onerror="img_not_found()">\
            </div>\
          </div>\
          <div class="col-5">\
            <div style="margin: unset; height:120px; max-height: 120px">\
              <img src="{img_url_2}" onerror="img_not_found()">\
            </div>\
          </div>'

var template_eventline_img_1 = '\
          <div class="col">\
            <div style="height:120px; max-height: 120px">\
              <img src="{img_url}" onerror="img_not_found()">\
            </div>\
          </div>'
  
var template_eventline = '\
  <div class="col-sm-6 col-md-4 col-lg-3">\
    <div class="eventline_item" id="{eventline_id}" value="{num_cur}">\
      <div class="eventline_img" style="padding:0 15px; background:#eee;">\
        <div class="row">\
          {html_img}\
        </div>\
      </div>\
      <div class="eventline_info">\
        <h4 title="{title}" id="title">{title}</h4>\
        <div class="date_info">\
          <div class="row">\
            <div class="col">\
              <span class="tag">最新日期</span>\
              <span>{time}</span>\
            </div>\
          </div>\
          <div class="row">\
            <div class="col">\
              <span class="tag tag-date">开始日期</span>\
              <span>{start_time}</span>\
            </div>\
          </div>\
        </div>\
        <div class="entity_info">\
          <div class="row"><div><span class="tag tag-location" style="margin-left:15px;">地点:</span>\
            </div>\
            <div class="col" style="padding-left:0">\
              <span class="location" title="地点:{locations}">{locations}</span>\
            </div>\
          </div>\
        </div>\
        <div class="keywords_info">\
          <div class="row"><div class="col"><span class="tag">关键词:</span><span>{keywords}</span></div></div>\
        </div>\
      </div>\
      <div class="row statistical-info">\
        <div class="col">\
          <div class="row"><div class="col"><span>{timeRange}热度</span></div></div>\
          <div class="row"><div class="col"><span id="num_cur" class="number">{num_cur}<span></div></div>\
        </div>\
        <div class="col">\
          <div class="row"><div class="col"><span>文章数</span></div></div>\
          <div class="row"><div class="col"><span id="num_doc" class="number">{num_doc}<span></div></div>\
        </div>\
        <div class="col">\
          <div class="row"><div class="col"><span class="item-label">事件数</span></div></div>\
          <div class="row"><div class="col"><span id="num_event" class="number">{num_event}</span></div></div>\
        </div>\
      </div>\
      <div class="row follow-me">\
        <div class="col unfollow">\
          <span>不感兴趣</span>\
        </div>\
        <div class="col follow">\
          <span>关注</span>\
        </div>\
      </div>\
    </div>\
  </div>';
  
String.prototype.format = function(args) {
  var result = this;
  if (arguments.length > 0) {
    if (arguments.length == 1 && typeof (args) == "object") {
      for (var key in args) {
        if(args[key]!=undefined){
          var reg = new RegExp("({" + key + "})", "g");
          result = result.replace(reg, args[key]);
        }
      }
    }else {
      for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] != undefined) {
          var reg= new RegExp("({)" + i + "(})", "g");
          result = result.replace(reg, arguments[i]);
        }
      }
    }
  }
  return result;
}

function time2str(ts){
  var d = new Date(ts);
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var date = d.getDate();
  var hour = d.getHours();
  var minute = d.getMinutes();
  var second = d.getSeconds();
  return year + '-' + to2digits(month) + '-' + to2digits(date) + 'T' + to2digits(hour) + ':'+to2digits(minute)+':' + to2digits(second); 
}

function to2digits(number){
  return number > 9 ? '' + number : '0' + number;
}


function correctImgUrl(imgUrl){
  if(imgUrl.indexOf('http') != 0)
    return 'http:' + imgUrl;
  return imgUrl;
}

function createNewEventline(params){
  var images = params.images;
  var length = images.length;
  var html_img = '';
  if(length==0){
    html_img = template_eventline_img_1.format({img_url:'./img/404.gif'});
  }else if(length==1){
    html_img = template_eventline_img_1.format({img_url:images[0]});
  }else if(length==2){
    html_img = template_eventline_img_2.format({img_url:images[0], img_url_2:images[1]});
  }else{
    html_img = template_eventline_img_3.format({img_url:images[0], img_url_2:images[1], img_url_3:images[2]});
  }
  params.html_img = html_img;
  return template_eventline.format(params);
}

function createNewNews(params){
  return template_news.format(params);
}

function initDatetimePicker(){
  var st;
  $("#historical_btn").hover(function(){
    clearTimeout(st);
    var x = $(this).offset().top;
    var y = $(this).offset().left;
    var height = $(this).height();
    $('.datetimepicker').css('top', x+height);
    $('.datetimepicker').css('left', y);
    $('.datetimepicker').css('display', 'block');
  }, function(){
    st = setTimeout(function(){$('.datetimepicker').css('display', 'none')}, 300);
  });
  
  $('.datetimepicker').hover(function(){
    if(is_focus)
      return;
    clearTimeout(st);
    var x = $("#historical_btn").offset().top;
    var y = $("#historical_btn").offset().left;
    var height = $("#historical_btn").height();
    $('.datetimepicker').css('top', x+height);
    $('.datetimepicker').css('left', y);
    $('.datetimepicker').css('display', 'block');
  }, function(){
    if(is_focus)
      return;
    $('.datetimepicker').css('display', 'none');
  });
  
  var is_focus = false;
  $('.datetimepicker input').focus(function(){
    is_focus = true;
  });
  
  $('.datetimepicker input').blur(function(){
    is_focus = false;
  });
  $('#time_confirm_btn').on('click', function(){
    start_time = $('#start_time').val();
    end_time = $('#end_time').val();
    if(start_time > end_time){
      alert("开始时间不能大于结束时间");
      return;
    }
    window.location.href="./?limit=25&start_time=" + start_time + "&end_time=" + end_time;
  });
}

function img_not_found(){
  var img = event.srcElement;
  img.style.opacity=0;
  img.onerror=null;
}

function getParameter(key) {
  if (key in paramMap)
    return paramMap[key];
  return null;
}

var splits = decodeURIComponent(window.location.search).substr(1).split('&');
for(var i=0; i<splits.length; i++){
  var item = splits[i].split('=');
  paramMap[item[0]]=item[1];
}

